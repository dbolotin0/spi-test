#include <avr/wdt.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <string.h>
#include <RF24.hpp>

#include "uart.h"
#include "spi.h"

void __transmit_packet(uint8_t* data_out, uint8_t* data_in, uint8_t len) {
    if (data_out == NULL && data_in == NULL)
        for (uint8_t i = 0; i < len; ++i)
            spi_transmit(0x00);
    if (data_out == NULL)
        for (uint8_t i = 0; i < len; ++i)
            data_in[i] = spi_transmit(0x00);
    else if (data_in == NULL)
        for (uint8_t i = 0; i < len; ++i)
            spi_transmit(data_out[i]);
    else
        for (uint8_t i = 0; i < len; ++i)
            data_in[i] = spi_transmit(data_out[i]);
}

class RF24_HW {
public:
    void ce_high() const {
        PORTB |= _BV(PB0);
    }

    void ce_low() const {
        PORTB &= ~_BV(PB0);
    }

    uint8_t transmit_single(uint8_t data_out) const {
        spi_ss_low();
        uint8_t val = spi_transmit(0xFF);
        spi_ss_high();
        return val;
    }

    void transmit_packet(uint8_t* data_out, uint8_t* data_in, uint8_t len) const {
        spi_ss_low();
        __transmit_packet(data_out, data_in, len);
        spi_ss_high();
    }

    void transmit_2packet(uint8_t* data_out1, uint8_t* data_in1, uint8_t len1,
            uint8_t* data_out2, uint8_t* data_in2, uint8_t len2) const {
        spi_ss_low();
        __transmit_packet(data_out1, data_in1, len1);
        __transmit_packet(data_out2, data_in2, len2);
        spi_ss_high();
    }

    void inline delay_us(uint16_t delay) const {
        _delay_us(delay);
    }
};

int main(int argc, char** argv) {
    DDRB |= _BV(PB0);

    //Setting up serial interfaces
    init_uart();
    init_spi();

    //    uint8_t g;
    //    while (true) {
    //        ce.set();
    //        g = spi.transmit_single(0);
    //        printf("D:%X", g);
    //        ce.clear();
    //        g = spi.transmit_single(0);
    //        printf("D:%X", g);
    //        _delay_ms(500);
    //    }

    printf("Before init.\n");

    RF24_HW hw;
    RF24<RF24_HW> rf24(hw);
    rf24.init();
    rf24.setRXAddress(0, 0xA2A98FD634L);
    uint64_t address = rf24.getRXAddress(0);
    rf24.setPayloadSize(0, 1);

    printf("Address LOW: %lX \n", (uint32_t) address);
    printf("Address HIGH: %lX \n", (uint32_t) (address >> 32));

    rf24.startListening();

    bool a;
    uint8_t msg[1];
    uint8_t os = 0, cs, of = 0, cf;
    while (true) {
        if (rf24.available()) {
            do {
                a = rf24.read_payload(msg, 1);
                printf("Received: %.2X\n", msg[0]);
            } while (a);
        }
        
        cs = rf24.getStatus();
        if (cs != os) {
            printf("New Status: %.2X\n", cs);
            os = cs;
        }

        cf = rf24.read_register(FIFO_STATUS);
        if (cf != of) {
            printf("New FIFO: %.2X\n", cf);
            of = cf;
        }

        _delay_ms(5);
    }

    //    uint8_t msg[] = {0xDE};
    //    uint8_t g = 0;
    //
    //    while (true) {
    //        ++g;
    //        msg[0] = g;
    //        bool success = rf24.trySend(msg, 1);
    //
    //        if (success)
    //            printf("Sent. %X\n", msg[0]);
    //        else
    //            printf("Fail. %X\n", msg[0]);
    //        
    //        printf("FIFO: %X\n", rf24.read_register(FIFO_STATUS));
    //        printf("Status: %X\n", rf24.getStatus());
    //
    //        _delay_ms(500);
    //    }

    return 0;
}


