/* 
 * File:   spi.h
 * Author: dbolotin
 *
 * Created on June 14, 2014, 4:48 PM
 */

#ifndef SPI_H
#define	SPI_H

#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

    void init_spi();
    uint8_t spi_transmit(uint8_t data);
    void spi_ss_low();
    void spi_ss_high();

#ifdef	__cplusplus
}
#endif

#endif	/* SPI_H */

