#include "spi.h"

#include <avr/io.h>

#define DDR_SPI DDRB
#define PORT_SPI PORTB

#define DD_SS PB2
#define DD_MOSI PB3
#define DD_MISO PB4
#define DD_SCK PB5

void init_spi(void) {
    /* Set MOSI and SCK output, all others input */
    DDR_SPI |= _BV(DD_MOSI) | _BV(DD_SCK) | _BV(DD_SS);
    
    /* Enable SPI, Master, set clock rate fck/16 */
    SPCR = _BV(SPE) | _BV(MSTR) | _BV(SPR0);
    
    /* Setting chip select pin to high state */
    spi_ss_high();
}

uint8_t spi_transmit(uint8_t data) {
    /* Start transmission */
    SPDR = data;
    
    /* Wait for transmission complete */
    loop_until_bit_is_set(SPSR, SPIF);
    
    /* Getting received data */
    return SPDR;
}

void spi_ss_low() {
    PORT_SPI &= ~_BV(DD_SS);
}

void spi_ss_high() {
    PORT_SPI |= _BV(DD_SS);
}
