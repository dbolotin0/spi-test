/* 
 * File:   UART.h
 * Author: dbolotin
 *
 * Created on June 14, 2014, 12:28 PM
 */

#ifndef UART_H
#define	UART_H

#ifdef __cplusplus
extern "C" {
#endif

    void init_uart();

#ifdef __cplusplus
}
#endif

#endif	/* UART_H */

