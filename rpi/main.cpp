/* 
 * File:   main.cpp
 * Author: dbolotin
 *
 * Created on June 13, 2014, 12:10 PM
 */
#include "spi_rpi.h"
#include "gpio_rpi.h"
#include <RF24.hpp>
#include <time.h>
#include <string.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

class RF24_HW {
    const SPI_RPi & spi;
    const GPIO_RPi & gpio;
    const uint8_t pin;
public:
    RF24_HW(const SPI_RPi & spi, GPIO_RPi const & gpio,
            const uint8_t &pin) :
    spi(spi), gpio(gpio), pin(pin) {
    }

    void ce_high() const {
        gpio.set(pin);
    }

    void ce_low() const {
        gpio.clear(pin);
    }

    uint8_t transmit_single(uint8_t data_out) const {
        return spi.transmit_single(data_out);
    }

    void transmit_packet(uint8_t* data_out, uint8_t* data_in, uint8_t len) const {
        spi.transmit_packet(data_out, data_in, len);
    }

    void transmit_2packet(uint8_t* data_out1, uint8_t* data_in1, uint8_t len1,
            uint8_t* data_out2, uint8_t* data_in2, uint8_t len2) const {
        spi.transmit_packet(data_out1, data_in1, len1,
                data_out2, data_in2, len2);
    }

    void inline delay_us(uint16_t delay) const {
        struct timespec tim;
        tim.tv_sec = 0;
        tim.tv_nsec = 1000L * delay;
        nanosleep(&tim, NULL);
    }
};

int main(int argc, char** argv) {
    struct timespec tim;
    tim.tv_sec = 0;
    tim.tv_nsec = 500000000L;


    GPIO_RPi gpio;
    gpio.open();
    gpio.set_output(25);

    SPI_RPi spi("/dev/spidev0.0", SPI_MODE_0, 8, 500000);
    spi.open();

    PIN_RPi pin(gpio, 25);
    pin.clear();

    RF24<SPI_RPi, PIN_RPi> rf24(spi, pin);
    rf24.init();
    rf24.setTXAddress(0xA2A98FD634L);
    //rf24.setRXAddress(0, 0xA2A98FD634L);
    //rf24.setPayloadSize(0, 1);

    uint8_t val = rf24.getStatus();
    printf("%.2X\n", val);

    uint64_t address = rf24.getTXAddress();
    printf("%llX\n", address);

    //address = rf24.getTXAddress();
    //printf("%llX\n", address);

    //    uint8_t msg[] = {0xDE};
    //
    //    while (true) {
    //        bool success = rf24.trySend(msg, 1);
    //        if (success)
    //            printf("Sent.\n");
    //        else
    //            printf("Fail.\n");
    //        nanosleep(&tim, NULL);
    //    }

    //    rf24.startListening();
    //
    //    tim.tv_sec = 0;
    //    tim.tv_nsec = 5000000L;
    //    bool a;
    //    uint8_t msg[1];
    //    uint8_t os, cs;
    //    while (true) {
    //        if (rf24.available()) {
    //            do{
    //                a = rf24.read_payload(msg, 1);
    //                printf("Received: %.2X\n", msg[0]);
    //            } while(a);
    //        }
    //        cs = rf24.getStatus();
    //        if (cs != os) {
    //            printf("New Status: %.2X\n", cs);
    //            os = cs;
    //        }
    //        nanosleep(&tim, NULL);
    //    }

    uint8_t msg[] = {0xDE};
    uint8_t g = 0;

    while (true) {
        ++g;
        msg[0] = g;
        bool success = rf24.trySend(msg, 1);

        if (success)
            printf("Sent. %X\n", msg[0]);
        else
            printf("Fail. %X\n", msg[0]);

        printf("FIFO: %X\n", rf24.read_register(FIFO_STATUS));
        printf("Status: %X\n", rf24.getStatus());

        nanosleep(&tim, NULL);
    }

    //    srand(time(NULL));
    //    uint8_t size = 100;
    //
    //    uint8_t* out = new uint8_t[size];
    //    for (int i = 0; i < size; ++i){
    //        out[i] = (uint8_t) rand();
    //    }
    //
    //    uint8_t* in = new uint8_t[size];
    //
    //    spi.transmit(out, in, size, 0);
    //
    //    if (memcmp(in, out, size) == 0)
    //        printf("Ok!\n");
    //    else
    //        printf("Error!\n");
    //
    //    delete in;
    //    delete out;


    //
    //
    //
    //    uint8_t val, val1 = 0x55;
    //
    //    spi.transmit(&val1, &val, 1, 0);
    //
    //    printf("%.2X\n", val);

    return 0;
}

