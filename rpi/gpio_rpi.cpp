#include "gpio_rpi.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0

GPIO_RPi::GPIO_RPi() {
}

GPIO_RPi::~GPIO_RPi() {
}

bool GPIO_RPi::open() {
    int mem_fd;
    void *gpio_map;

    /* open /dev/mem */
    if ((mem_fd = ::open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
        perror("Can't open /dev/mem.");
        return false;
    }

    /* mmap GPIO */
    gpio_map = mmap(
            NULL, //Any adddress in our space will do
            BLOCK_SIZE, //Map length
            PROT_READ | PROT_WRITE, // Enable reading & writting to mapped memory
            MAP_SHARED, //Shared with other processes
            mem_fd, //File to map
            GPIO_BASE //Offset to GPIO peripheral
            );

    ::close(mem_fd); //No need to keep mem_fd open after mmap

    if (gpio_map == MAP_FAILED) {
        perror("Mmap error.");
        return false;
    }

    // Always use volatile pointer!
    gpio = (volatile unsigned *) gpio_map;

    return true;
}

void GPIO_RPi::set_output(uint8_t pin) const {
    INP_GPIO(pin); // must use INP_GPIO before we can use OUT_GPIO
    OUT_GPIO(pin);
}

void GPIO_RPi::set(uint8_t pin) const {
    GPIO_SET = 1 << pin;
}

void GPIO_RPi::clear(uint8_t pin) const {
    GPIO_CLR = 1 << pin;
}

PIN_RPi::PIN_RPi(const GPIO_RPi& gpio, const uint8_t& pin) : gpio(gpio), pin(pin) {
}

void PIN_RPi::set() const {
    gpio.set(pin);
}

void PIN_RPi::clear() const {
    gpio.clear(pin);
}