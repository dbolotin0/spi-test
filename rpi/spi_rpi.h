/* 
 * File:   spi_rpi.h
 * Author: dbolotin
 *
 * Created on June 13, 2014, 1:53 PM
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#ifndef SPI_RPI_H
#define	SPI_RPI_H

class SPI_RPi {
    /*
     * File name of SPI device.
     */
    const char *deviceName;
    /*
     * SPI mode
     */
    const uint8_t mode;
    /*
     * Bits per word.
     */
    const uint8_t bits;
    /*
     * Max transmission speed
     */
    const uint32_t speed;
    /*
     * File descriptor.
     */
    int fd;

public:
    /*
     * Constructor
     */
    SPI_RPi(const char *fileName, uint8_t mode, const uint8_t bits, const uint32_t speed);

    /*
     * Destructor
     */
    ~SPI_RPi();

    /*
     * Main transmit methods
     */
    uint8_t transmit_single(uint8_t data_out) const;

    void transmit_packet(uint8_t* data_out, uint8_t* data_in, uint8_t len) const;

    void transmit_2packet(uint8_t* data_out1, uint8_t* data_in1, uint8_t len1,
            uint8_t* data_out2, uint8_t* data_in2, uint8_t len2) const;
    
    void transmit_3packet(uint8_t* data_out1, uint8_t* data_in1, uint8_t len1,
        uint8_t* data_out2, uint8_t* data_in2, uint8_t len2,
        int8_t* data_out3, uint8_t* data_in3, uint8_t len3) const;

    /*
     * Open device
     */
    bool open();

    /*
     * Close device
     */
    void close();
};

#endif	/* SPI_RPI_H */

