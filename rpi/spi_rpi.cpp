/* 
 * File:   spi_rpi.cpp
 * Author: dbolotin
 * 
 * Created on June 13, 2014, 1:53 PM
 */

#include "spi_rpi.h"

SPI_RPi::SPI_RPi(const char* fileName, uint8_t mode, const uint8_t bits, const uint32_t speed) :
deviceName(fileName), mode(mode), bits(bits), speed(speed) {
}

SPI_RPi::~SPI_RPi() {
    close();
}

bool SPI_RPi::open() {
    uint32_t check32;
    uint8_t check8;
    int ret = 0;

    fd = ::open(deviceName, O_RDWR);
    if (fd < 0) {
        perror("Can't open device.");
        return false;
    }

    /*
     * SPI mode
     */
    ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    if (ret == -1) {
        perror("Can't set SPI mode.");
        return false;
    }

    ret = ioctl(fd, SPI_IOC_RD_MODE, &check8);
    if (ret == -1) {
        perror("Can't get SPI mode.");
        return false;
    }

    if (mode != check8) {
        perror("Wrong mode.");
        return false;
    }

    /*
     * Bits per word
     */
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1) {
        perror("Can't set bits per word");
        return false;
    }

    ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &check8);
    if (ret == -1) {
        perror("Can't get bits per word");
        return false;
    }

    if (bits != check8) {
        perror("Wrong bits per word.");
        return false;
    }


    /*
     * Max speed hz
     */
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1) {
        perror("Can't set max speed hz");
        return false;
    }

    ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &check32);
    if (ret == -1) {
        perror("Can't get max speed hz");
        return false;
    }

    if (speed != check32) {
        perror("Wrong speed.");
        return false;
    }

    return true;
}

void SPI_RPi::close() {
    ::close(fd);
}

//bool SPI_RPi::transmit(uint8_t* data_out, uint8_t* data_in, uint8_t len, uint16_t delay) {
//    struct spi_ioc_transfer tr = {
//        (__u64) data_out,
//        (__u64) data_in,
//        len,
//        0,
//        speed,
//        bits,
//        delay
//    };
//
//    int ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
//    if (ret < 1){
//        perror("Can't send SPI message.");
//        return false;
//    }
//    
//    return true;
//}

uint8_t SPI_RPi::transmit_single(uint8_t data_out) const {
    uint8_t data_in;

    struct spi_ioc_transfer tr;
    tr.tx_buf = (__u64) & data_out;
    tr.rx_buf = (__u64) & data_in;
    tr.len = 1;
    tr.speed_hz = speed;
    tr.delay_usecs = 0;
    tr.bits_per_word = bits;
    tr.cs_change = false;
    tr.pad = 0;
    
    int ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
    if (ret < 1) {
        perror("Can't send SPI message.");
        return false;
    }

    return data_in;
}

void SPI_RPi::transmit_packet(uint8_t* data_out, uint8_t* data_in, uint8_t len) const {
    struct spi_ioc_transfer tr;
    
    tr.tx_buf = (__u64) data_out;
    tr.rx_buf = (__u64) data_in;
    tr.len = len;
    tr.speed_hz = speed;
    tr.delay_usecs = 0;
    tr.bits_per_word = bits;
    tr.cs_change = false;
    tr.pad = 0;

    int ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
    if (ret < 1) {
        perror("Can't send SPI message.");
    }
}

void SPI_RPi::transmit_2packet(uint8_t* data_out1, uint8_t* data_in1, uint8_t len1,
        uint8_t* data_out2, uint8_t* data_in2, uint8_t len2) const {
    struct spi_ioc_transfer tr[2];

    tr[0].tx_buf = (__u64) data_out1; // Can be NULL
    tr[0].rx_buf = (__u64) data_in1; // Can be NULL
    tr[0].len = len1;
    tr[0].bits_per_word = bits;
    tr[0].speed_hz = speed;
    tr[0].delay_usecs = 0;
    tr[0].pad = 0;
    tr[0].cs_change = false;

    tr[1].tx_buf = (__u64) data_out2; // Can be NULL
    tr[1].rx_buf = (__u64) data_in2; // Can be NULL
    tr[1].len = len2;
    tr[1].bits_per_word = bits;
    tr[1].speed_hz = speed;
    tr[1].delay_usecs = 0;
    tr[1].pad = 0;
    tr[1].cs_change = false;

    int ret = ioctl(fd, SPI_IOC_MESSAGE(2), tr);
    if (ret < 1) {
        perror("Can't send SPI message.");
    }
}

void SPI_RPi::transmit_3packet(uint8_t* data_out1, uint8_t* data_in1, uint8_t len1,
        uint8_t* data_out2, uint8_t* data_in2, uint8_t len2,
        int8_t* data_out3, uint8_t* data_in3, uint8_t len3) const {
    struct spi_ioc_transfer tr[3];

    tr[0].tx_buf = (__u64) data_out1; // Can be NULL
    tr[0].rx_buf = (__u64) data_in1; // Can be NULL
    tr[0].len = len1;
    tr[0].bits_per_word = bits;
    tr[0].speed_hz = speed;
    tr[0].delay_usecs = 0;
    tr[0].pad = 0;
    tr[0].cs_change = false;

    tr[1].tx_buf = (__u64) data_out2; // Can be NULL
    tr[1].rx_buf = (__u64) data_in2; // Can be NULL
    tr[1].len = len2;
    tr[1].bits_per_word = bits;
    tr[1].speed_hz = speed;
    tr[1].delay_usecs = 0;
    tr[1].pad = 0;
    tr[1].cs_change = false;
    
    tr[2].tx_buf = (__u64) data_out3; // Can be NULL
    tr[2].rx_buf = (__u64) data_in3; // Can be NULL
    tr[2].len = len3;
    tr[2].bits_per_word = bits;
    tr[2].speed_hz = speed;
    tr[2].delay_usecs = 0;
    tr[2].pad = 0;
    tr[2].cs_change = false;

    int ret = ioctl(fd, SPI_IOC_MESSAGE(3), tr);
    if (ret < 1) {
        perror("Can't send SPI message.");
    }
}