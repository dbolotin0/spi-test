/* 
 * File:   gpio_rpi.h
 * Author: dbolotin
 *
 * Created on June 13, 2014, 4:13 PM
 */

#include <stdint.h>

#ifndef GPIO_RPI_H
#define	GPIO_RPI_H

class GPIO_RPi {
    // I/O access
    volatile unsigned *gpio;

public:
    GPIO_RPi();
    ~GPIO_RPi();
    bool open();
    void set_output(uint8_t pin) const;
    void set(uint8_t pin) const;
    void clear(uint8_t pin) const;
};

class PIN_RPi {
    GPIO_RPi const & gpio;
    const uint8_t pin;
public:
    PIN_RPi(GPIO_RPi const & gpio, const uint8_t &pin);
    void set() const;
    void clear() const;
};

#endif	/* GPIO_RPI_H */

