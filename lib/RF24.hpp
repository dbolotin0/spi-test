/* 
 * File:   NRF24.h
 * Author: dbolotin
 *
 * Created on June 13, 2014, 4:46 PM
 * 
 * Adopted from RF24 lib by maniacbug (https://github.com/maniacbug)
 * https://github.com/maniacbug/RF24
 */

#ifndef RF24_H
#define	RF24_H

#include <stdio.h>
#include <stddef.h>
#include <math.h>

/* Memory Map */
#define CONFIG      ((uint8_t) 0x00)
#define EN_AA       ((uint8_t) 0x01)
#define EN_RXADDR   ((uint8_t) 0x02)
#define SETUP_AW    ((uint8_t) 0x03)
#define SETUP_RETR  ((uint8_t) 0x04)
#define RF_CH       ((uint8_t) 0x05)
#define RF_SETUP    ((uint8_t) 0x06)
#define STATUS      ((uint8_t) 0x07)
#define OBSERVE_TX  ((uint8_t) 0x08)
#define CD          ((uint8_t) 0x09)
#define RX_ADDR_P0  ((uint8_t) 0x0A)
#define RX_ADDR_P1  ((uint8_t) 0x0B)
#define RX_ADDR_P2  ((uint8_t) 0x0C)
#define RX_ADDR_P3  ((uint8_t) 0x0D)
#define RX_ADDR_P4  ((uint8_t) 0x0E)
#define RX_ADDR_P5  ((uint8_t) 0x0F)
#define TX_ADDR     ((uint8_t) 0x10)
#define RX_PW_P0    ((uint8_t) 0x11)
#define RX_PW_P1    ((uint8_t) 0x12)
#define RX_PW_P2    ((uint8_t) 0x13)
#define RX_PW_P3    ((uint8_t) 0x14)
#define RX_PW_P4    ((uint8_t) 0x15)
#define RX_PW_P5    ((uint8_t) 0x16)
#define FIFO_STATUS ((uint8_t) 0x17)
#define DYNPD	    ((uint8_t) 0x1C)
#define FEATURE	    ((uint8_t) 0x1D)

/* Bit Mnemonics */
#define MASK_RX_DR  6
#define MASK_TX_DS  5
#define MASK_MAX_RT 4
#define EN_CRC      3
#define CRCO        2
#define PWR_UP      1
#define PRIM_RX     0
#define ENAA_P5     5
#define ENAA_P4     4
#define ENAA_P3     3
#define ENAA_P2     2
#define ENAA_P1     1
#define ENAA_P0     0
#define ERX_P5      5
#define ERX_P4      4
#define ERX_P3      3
#define ERX_P2      2
#define ERX_P1      1
#define ERX_P0      0
#define AW          0
#define ARD         4
#define ARC         0
#define PLL_LOCK    4
#define RF_DR       3
#define RF_PWR      6
#define RX_DR       6
#define TX_DS       5
#define MAX_RT      4
#define RX_P_NO     1
#define TX_FULL     0
#define PLOS_CNT    4
#define ARC_CNT     0
#define TX_REUSE    6
#define FIFO_FULL   5
#define TX_EMPTY    4
#define RX_FULL     1
#define RX_EMPTY    0
#define DPL_P5	    5
#define DPL_P4	    4
#define DPL_P3	    3
#define DPL_P2	    2
#define DPL_P1	    1
#define DPL_P0	    0
#define EN_DPL	    2
#define EN_ACK_PAY  1
#define EN_DYN_ACK  0

/* Instruction Mnemonics */
#define R_REGISTER    ((uint8_t) 0x00)
#define W_REGISTER    ((uint8_t) 0x20)
#define REGISTER_MASK ((uint8_t) 0x1F)
#define ACTIVATE      ((uint8_t) 0x50)
#define R_RX_PL_WID   ((uint8_t) 0x60)
#define R_RX_PAYLOAD  ((uint8_t) 0x61)
#define W_TX_PAYLOAD  ((uint8_t) 0xA0)
#define W_ACK_PAYLOAD ((uint8_t) 0xA8)
#define FLUSH_TX      ((uint8_t) 0xE1)
#define FLUSH_RX      ((uint8_t) 0xE2)
#define REUSE_TX_PL   ((uint8_t) 0xE3)
#define NOP           ((uint8_t) 0xFF)

/* Non-P omissions */
#define LNA_HCURR   0

/* P model memory Map */
#define RPD         0x09

/* P model bit Mnemonics */
#define RF_DR_LOW   5
#define RF_DR_HIGH  3
#define RF_PWR_LOW  1
#define RF_PWR_HIGH 2

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define BIT(bit) (1 << (bit))

typedef enum {
    RF24_PA_MIN = 0, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX, RF24_PA_ERROR
} rf24_pa_dbm_e;

typedef enum {
    RF24_1MBPS = 0, RF24_2MBPS, RF24_250KBPS
} rf24_datarate_e;

typedef enum {
    RF24_CRC_DISABLED = 0, RF24_CRC_8, RF24_CRC_16
} rf24_crclength_e;

template <class HW>
class RF24 {
private:
    const HW & hw;
    
    uint8_t __cmd(uint8_t cmd, uint8_t* out_buf, uint8_t* in_buf, uint8_t len) {
        uint8_t status;

        hw.transmit_2packet(&cmd, &status, 1,
                out_buf, in_buf, len);

        return status;
    }

public:
    
    RF24(const HW & hw) : hw(hw) {
    }

    uint8_t read_register(uint8_t reg, uint8_t* buf, uint8_t len) {
        return __cmd((R_REGISTER | (REGISTER_MASK & reg)), NULL, buf, len);
    }

    uint8_t write_register(uint8_t reg, uint8_t* buf, uint8_t len) {
        return __cmd((W_REGISTER | (REGISTER_MASK & reg)), buf, NULL, len);
    }

    uint8_t read_register(uint8_t reg) {
        uint8_t data_out[2];
        data_out[0] = (R_REGISTER | (REGISTER_MASK & reg));
        data_out[1] = 0x00;
        uint8_t data_in[2] = {0,};

        hw.transmit_packet(data_out, data_in, 2);

        return data_in[1];
    }

    uint8_t write_register(uint8_t reg, uint8_t value) {
        uint8_t data_out[2];
        data_out[0] = (W_REGISTER | (REGISTER_MASK & reg));
        data_out[1] = value;
        uint8_t data_in[2] = {0,};

        hw.transmit_packet(data_out, data_in, 2);

        return data_in[0];
    }

    void toggle_features(void) {
        uint8_t data[] = {ACTIVATE, 0x73};
        hw.transmit_packet(data, NULL, 2);
    }

    uint8_t flush_rx(void) {
        return hw.transmit_single(FLUSH_RX);
    }

    uint8_t flush_tx(void) {
        return hw.transmit_single(FLUSH_TX);
    }

    void init() {
        hw.ce_low();
        
        hw.delay_us(5000);
        
        write_register(CONFIG, 0x0);

        setPALevel(RF24_PA_MAX);

        setDataRate(RF24_250KBPS);

        setCRCLength(RF24_CRC_16);

        setChannel(76);

        write_register(STATUS, BIT(RX_DR) | BIT(TX_DS) | BIT(MAX_RT));

        // Flush buffers
        flush_rx();
        flush_tx();
    }

    uint8_t write_payload(void* buf, uint8_t len) {
        return __cmd(W_TX_PAYLOAD, 
                reinterpret_cast<uint8_t*> (buf), NULL, len);
    }

    bool read_payload(void* buf, uint8_t len) {
        __cmd(R_RX_PAYLOAD,
                NULL, reinterpret_cast<uint8_t*> (buf), len);
        
        write_register(STATUS, BIT(RX_DR));
        
        return !(read_register(FIFO_STATUS) & BIT(RX_EMPTY));
    }

    uint8_t getStatus(void) {
        return hw.transmit_single(NOP);
    }

    uint8_t setRXAddress(uint8_t pipe, uint64_t address) {
        uint8_t buf[5];

        for (int i = 0; i < 5; ++i)
            buf[i] = (uint8_t) (address >> (i * 8));

        return write_register(RX_ADDR_P0 + pipe, buf, 5);
    }
    
    uint64_t getRXAddress(uint8_t pipe) {
        uint8_t buf[5];
        read_register(RX_ADDR_P0 + pipe, buf, 5);
        uint64_t address = 0;

        for (int i = 0; i < 5; ++i)
            address |= ((uint64_t) buf[i]) << (i * 8);

        return address;
    }

    uint8_t setTXAddress(uint64_t address) {
        uint8_t buf[5];

        for (int i = 0; i < 5; ++i)
            buf[i] = (uint8_t) (address >> (i * 8));

        return write_register(TX_ADDR, buf, 5);
    }

    uint64_t getTXAddress() {
        uint8_t buf[5];
        read_register(TX_ADDR, buf, 5);
        uint64_t address = 0;

        for (int i = 0; i < 5; ++i)
            address |= ((uint64_t) buf[i]) << (i * 8);

        return address;
    }

    void startListening(void) {
        write_register(CONFIG, read_register(CONFIG) | BIT(PWR_UP) | BIT(PRIM_RX));
        write_register(STATUS, BIT(RX_DR) | BIT(TX_DS) | BIT(MAX_RT));

        // Flush buffers
        flush_rx();
        flush_tx();

        // Go!
        hw.ce_high();
        
        hw.delay_us(130);
    }

    void stopListening(void) {
        hw.ce_low();
    }

    bool trySend(uint8_t* buf, uint8_t len) {

        flush_tx();

        write_register(CONFIG, (read_register(CONFIG) | BIT(PWR_UP)) & ~BIT(PRIM_RX));
        
        hw.delay_us(150);
        
        write_payload(buf, len);
        
        write_register(STATUS, BIT(RX_DR) | BIT(TX_DS) | BIT(MAX_RT));

        hw.ce_high();
        hw.delay_us(15);
        hw.ce_low();

        uint8_t status;
        bool result;
        while (true) {

            status = getStatus();

            if (status & BIT(MAX_RT)) {
                result = false;
                break;
            }

            if (status & BIT(TX_DS)) {
                result = true;
                break;
            }
        }

        write_register(STATUS, BIT(RX_DR) | BIT(TX_DS) | BIT(MAX_RT));

        write_register(CONFIG, read_register(CONFIG) & ~BIT(PWR_UP));
        
        if (!result)
            flush_tx();

        return result;
    }

    void setChannel(uint8_t channel) {
        write_register(RF_CH, channel);
    }

    void setPayloadSize(uint8_t pipe, uint8_t size) {
        write_register(RX_PW_P0 + pipe, size);
    }

    void setPALevel(rf24_pa_dbm_e level) {
        uint8_t setup = read_register(RF_SETUP);
        setup &= ~(BIT(RF_PWR_LOW) | BIT(RF_PWR_HIGH));

        // switch uses RAM (evil!)
        if (level == RF24_PA_MAX) {
            setup |= (BIT(RF_PWR_LOW) | BIT(RF_PWR_HIGH));
        } else if (level == RF24_PA_HIGH) {
            setup |= BIT(RF_PWR_HIGH);
        } else if (level == RF24_PA_LOW) {
            setup |= BIT(RF_PWR_LOW);
        } else if (level == RF24_PA_MIN) {
            // nothing
        } else if (level == RF24_PA_ERROR) {
            // On error, go to maximum PA
            setup |= (BIT(RF_PWR_LOW) | BIT(RF_PWR_HIGH));
        }

        write_register(RF_SETUP, setup);
    }

    rf24_pa_dbm_e getPALevel(void) {
        rf24_pa_dbm_e result = RF24_PA_ERROR;
        uint8_t power = read_register(RF_SETUP) & (BIT(RF_PWR_LOW) | BIT(RF_PWR_HIGH));

        // switch uses RAM (evil!)
        if (power == (BIT(RF_PWR_LOW) | BIT(RF_PWR_HIGH))) {
            result = RF24_PA_MAX;
        } else if (power == BIT(RF_PWR_HIGH)) {
            result = RF24_PA_HIGH;
        } else if (power == BIT(RF_PWR_LOW)) {
            result = RF24_PA_LOW;
        } else {
            result = RF24_PA_MIN;
        }

        return result;
    }

    void setDataRate(rf24_datarate_e speed) {
        uint8_t setup = read_register(RF_SETUP);

        // HIGH and LOW '00' is 1Mbs - our default
        setup &= ~(BIT(RF_DR_LOW) | BIT(RF_DR_HIGH));
        if (speed == RF24_250KBPS) {
            // Must set the RF_DR_LOW to 1; RF_DR_HIGH (used to be RF_DR) is already 0
            // Making it '10'.
            setup |= BIT(RF_DR_LOW);
        } else {
            // Set 2Mbs, RF_DR (RF_DR_HIGH) is set 1
            // Making it '01'
            if (speed == RF24_2MBPS) {
                setup |= BIT(RF_DR_HIGH);
            }
        }
        write_register(RF_SETUP, setup);
    }

    void setCRCLength(rf24_crclength_e length) {
        uint8_t config = read_register(CONFIG) & ~(BIT(CRCO) | BIT(EN_CRC));

        // switch uses RAM (evil!)
        if (length == RF24_CRC_DISABLED) {
            // Do nothing, we turned it off above. 
        } else if (length == RF24_CRC_8) {
            config |= BIT(EN_CRC);
        } else {
            config |= BIT(EN_CRC);
            config |= BIT(CRCO);
        }
        write_register(CONFIG, config);
    }

    bool available() {
        //return !(read_register(FIFO_STATUS) & BIT(RX_EMPTY));
        return getStatus() & BIT(RX_DR);
    }
};

#endif	/* NRF24_H */
